import Binace
import BX
import Poloniex


def GetBrokerList():
    try:
        brokList =  {
            #'bx':{'ETH':'https://bx.in.th/api/orderbook/?pairing=21','XRP':'https://bx.in.th/api/orderbook/?pairing=25','LTC':'https://bx.in.th/api/orderbook/?pairing=30','BCH':'https://bx.in.th/api/orderbook/?pairing=27','BSV':'https://bx.in.th/api/orderbook/?pairing=34','BTC':'https://bx.in.th/api/orderbook/?pairing=1','FEE':BX.GetFeePrice(),'BALANCE':BX.GetBalance('THB')},
            'binace':{'BSV':'https://www.binance.com/api/v1/depth?symbol=BCHSVUSDT','BCH':'https://www.binance.com/api/v1/depth?symbol=BCHABCUSDT','BTC':'https://www.binance.com/api/v1/depth?symbol=BTCUSDT','LTC':'https://www.binance.com/api/v1/depth?symbol=LTCUSDT','ETH':'https://www.binance.com/api/v1/depth?symbol=ETHUSDT','XRP':'https://www.binance.com/api/v1/depth?symbol=XRPUSDT','FEE':Binace.GetFeePrice(),'BALANCE':Binace.GetBalance('USDT')},
            'polo':{'BCH':'https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BCH','BTC':'https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC','LTC':'https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_LTC','ETH':'https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_ETH','XRP':'https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_XRP','FEE':Poloniex.GetFeePrice(),'BALANCE':Poloniex.GetBalance('USDT')},
            }

        return brokList
    except Exception as ex:
        print(str(ex) + ' at GetBrokerList()')
        