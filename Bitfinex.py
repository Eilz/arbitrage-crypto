import requests as req
import json
import time 
import sys
import re
import hmac
import hashlib
import json
from datetime import datetime
from decimal import Decimal
import decimal
import asyncio
import ApiKeyExchange  


BASE_URL = "https://api.bitfinex.com/"

def _nonce():
              
        return str(int(round(time.time() * 1000)))

def _headers(path, nonce, body):
        
        signature = "/api/" + path + nonce + body
        print ("Signing: " + signature)
        h = hmac.new(ApiKeyExchange.Bitfinex_SECRET.encode('utf8'), signature.encode('utf8'), hashlib.sha384)
        signature = h.hexdigest()

        return {
                "bfx-nonce": nonce,
                "bfx-apikey": ApiKeyExchange.Bitfinex_KEY,
                "bfx-signature": signature,
                "content-type": "application/json"
        }


def GetBalance(symbol):  
  try:
    # -------------- get Balance
    #parameter use :  currency  ,  amount 
    time.sleep(1)    
    nonce = _nonce()
    body = {}
    rawBody = json.dumps(body)
    path = "v2/auth/r/wallets"
    headers = _headers(path, nonce, rawBody)

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    for wallet in jsonCoverted:
            if wallet[1] ==symbol:
                    return float(wallet[2])

    return null
  except Exception as ex : 
        print(str(ex) + ' at GetBalance('+symbol+')')


def BuyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance):
  try:

    ApiKeyExchange.BX_KEY = 'a644812108d1'
    ApiKeyExchange.BX_SECRET = '6f5247214a65'
    resultBool = True
    pairing = getPairingId(sourceSymbol , tragetSymbol)
    rateBuy = getRateForBuy(sourceSymbol, tragetSymbol )
    amount = volumn
    if not isRebalance:
      amount  = GetBalance(sourceSymbol)

    time.sleep(1)
    
    BalanceSymbol = GetBalance(tragetSymbol)
    orderID = 0
    time.sleep(1)
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
    r = req.post('https://bx.in.th/api/order/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'pairing' : pairing , 'type' : 'buy' , 'amount' : amount , 'rate' : rateBuy })
    orderJson = json.loads(r.text)
    orderID = orderJson['order_id']
    
    time.sleep(1)       

    if GetBalance(tragetSymbol) <= BalanceSymbol:
      time.sleep(1)
      ApiKeyExchange.BX_KEY = 'a644812108d1'
      ApiKeyExchange.BX_SECRET = '6f5247214a65'
      nonce = datetime.now().strftime('%Y%m%d%H%M%S')
      signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
      h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
      r = req.post('https://bx.in.th/api/cancel/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'pairing' : pairing , 'order_id' : orderID })
      resultBool =  False
    #-------------
    return resultBool 
  except Exception as ex : 
      print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')



def transferOrder(symbol,tragetAccount, addressTag=""):
  try:
    ApiKeyExchange.BX_KEY = 'a644812108d1'
    ApiKeyExchange.BX_SECRET = '6f5247214a65'
    resultBool = False
    if(symbol == "XRP"):
      _tragetAccount = tragetAccount + "?dt=" + addressTag
    BalanceSymbol = GetBalance(symbol)
    time.sleep(1)  
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest() 
    
    if BalanceSymbol > 0:
      r = req.post('https://bx.in.th/api/withdrawal/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'currency' : symbol  , 'amount' : BalanceSymbol , 'address' : _tragetAccount })
      jsonConvert = json.loads(r.text)
      print(r.text)
      resultBool = (jsonConvert['success'])
      time.sleep(3) #< ----------------- timing for waiting withdraw

    return resultBool
  except Exception as ex : 
      print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')


def getPairingId(sourceSymbol , tragetSymbol):
  try:
    r = req.post('https://bx.in.th/api/pairing/')
    jsonload = json.loads(r.text)
    for pair in range(1,35):
      if str(pair) in jsonload:
        if jsonload[str(pair)]['primary_currency'] == sourceSymbol and jsonload[str(pair)]['secondary_currency'] == tragetSymbol:
          return str(jsonload[str(pair)]['pairing_id'])

    return "Null"
  except Exception as ex : 
      print(str(ex) + ' at getPairingId('+sourceSymbol+','+tragetSymbol+')')

 
def getRateForBuy(sourceSymbol, tragetSymbol ):
  try:
    time.sleep(1)
    pairing = getPairingId(sourceSymbol , tragetSymbol)
    r = req.post(url='https://bx.in.th/api/orderbook/?pairing='+pairing)
    jsonConvert = json.loads(r.text)
    rateBuy = float(jsonConvert['asks'][0][0]) * (110/100) 
    return rateBuy
  except Exception as ex : 
    print(str(ex) + ' at getRateForBuy('+sourceSymbol+','+tragetSymbol+')')

def getRateForSale(sourceSymbol, tragetSymbol ):
  try:
    time.sleep(1)
    pairing = getPairingId(sourceSymbol , tragetSymbol)
    r = req.post(url='https://bx.in.th/api/orderbook/?pairing='+pairing)
    jsonConvert = json.loads(r.text)
    rateSale = float(jsonConvert['bids'][0][0]) * (90/100) 
    return rateSale
  except Exception as ex : 
    print(str(ex) + ' at getRateForSale('+sourceSymbol+','+tragetSymbol+')')
 
def GetFeePrice():
  try:
    fee = {'tradeFee':0.2,
    'transfer':{
      'BTC':0.0004,
      'ETH':0.00135,
      'XRP':0.1,
      'OMG':0.1379,
      'LTC':0.001
    }
    
    }
    return fee
  except Exception as ex : 
    print(str(ex) + ' at GetFeePrice()')

def GetTradeStatus(sourceSymbol, tragetSymbol):
  try:
    r = req.post('https://bx.in.th/api/pairing/')
    jsonload = json.loads(r.text)
    for pair in jsonload:
      if pair['primary_currency'] == sourceSymbol and pair['secondary_currency'] == tragetSymbol:
        return pair['active']

    return False
  except Exception as ex : 
    print(str(ex) + ' at GetTradeStatus('+sourceSymbol+','+tragetSymbol+')')

def SellOrder(sourceSymbol , tragetSymbol): # น่าจะถูกละ
  try:
    key = 'a644812108d1'
    api_secret = '6f5247214a65'
    Balance = GetBalance(tragetSymbol)  
    time.sleep(1)
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    resultBool= True
    
    
    signature = key + nonce + api_secret
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
    
    CalRate = getRateForSale(sourceSymbol, tragetSymbol )
    
    pairKey = getPairingId(sourceSymbol , tragetSymbol )
    

    r = req.post('https://bx.in.th/api/order/', {'key': key ,'nonce': nonce,'signature': h , 'pairing' : int(pairKey) , 'type' : 'sell' , 'amount' : Balance , 'rate' : CalRate })
    orderJson = json.loads(r.text)
    orderID = orderJson['order_id']
    time.sleep(1)  
    if GetBalance(tragetSymbol) >= Balance:
      time.sleep(1)
      nonce = datetime.now().strftime('%Y%m%d%H%M%S')
      signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
      h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
      r = req.post('https://bx.in.th/api/cancel/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'pairing' : int(pairKey) , 'order_id' : orderID })
      resultBool =  False
  
    return resultBool
  except Exception as ex : 
    print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')


def getPriceBid():
  try:
    r = req.get('https://bx.in.th/api/orderbook/?pairing=21')
    orderJson = json.loads(r.text)
    return float(orderJson['bids'][0][0] ) - 1
  except Exception as ex : 
    print(str(ex) + ' at getPriceBid()')

 
def CheckPendingOrder(symbol):#CheckPendingOrder()
  try:
    time.sleep(1)
    ApiKeyExchange.BX_KEY = 'a644812108d1'
    ApiKeyExchange.BX_SECRET = '6f5247214a65'
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    #print(nonce)

    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()


    r = req.post('https://bx.in.th/api/withdrawal-history/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h })

    jsonConvert = json.loads(r.text)
    
    for withdraw in jsonConvert['withdrawals'][:10]:
      if withdraw['currency'] == symbol.upper() :
        return withdraw['withdrawal_status'] 
      
    return 'null'
  except Exception as ex : 
    print(str(ex) + ' at CheckPendingOrder('+symbol+')')  

def getMinimumSellSource(sourceSymbol):
  try:
    getText = req.get("https://api.bitfinex.com/v1/symbols_details")
    jsonConvertedRate = json.loads(getText.text)
    for pair in jsonConvertedRate:
       if pair["pair"] == sourceSymbol:
               return float( pair["minimum_order_size"])


    return 0
  except Exception as ex : 
    print(str(ex) + ' at getMinimumSellSource('+sourceSymbol+')')  

def getMinimumBuySource(tragetSymbol):
  try:
    getText = req.get("https://api.bitfinex.com/v1/symbols_details")
    jsonConvertedRate = json.loads(getText.text)
    for pair in jsonConvertedRate:
       if pair["pair"] == tragetSymbol:
               return float( pair["minimum_margin"])


    return 0
  except Exception as ex : 
    print(str(ex) + ' at getMinimumBuySource('+tragetSymbol+')')  



def GetAccount(symbol=""):
  try:
    if symbol == 'XRP':
      return "rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs","1015164727"
    elif symbol == 'ETH':
      return "0x68ca0a5C7DA2e5CA9c08104b78541e2596a4cf30",""
    elif symbol == 'LTC':
      return "LM7e2uKeVRatQWfNTsgVmLunii4WYpokKz",""

    return 'rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs',""
  except Exception as ex : 
    print(str(ex) + ' at GetAccount('+symbol+')')  


def getRateUsdUsdt():
    try:

        r = req.get("https://api.bitfinex.com/v1/book/ustusd")
        jsonText = json.loads(r.text)
        return  float(jsonText['asks'][0]['price'])
    except Exception as ex : 
        print(str(ex) + ' at getRateUsdUsdt()')



 