import requests as req
import json
import base64
import hmac
import hashlib
import time
import Lib.okex.account_api as okexAccount
import Lib.okex.spot_api as okexSpot



API_SECRET = 'D1799C5446C56094A3080378EC07EB3B'
API_Key = 'd1e7f3cb-105c-4bf4-8d9e-52d1e0773bd1'
API_PASSPHRASE = 'xinxere8407'
API_FUND = '7gUq69xJ'


key1 = "29e322f1-4054-40a7-a7df-b3c47c43182a"
skey1 = "2735B438B8F01B49BB2E8A26ADC6C39B"
pkey1 = 'xinxere8407'
fkey1 = 'nampapaya1212'

accountAPI = okexAccount.AccountAPI(API_Key, API_SECRET, 'xinxere8407', True)
spotAPI = okexSpot.SpotAPI(API_Key, API_SECRET, 'xinxere8407', True)



def GetBalanceFund(symbol):
  try:
   time.sleep(1)
   currentTime = req.get('https://www.okex.com/api/general/v3/time')
   y = json.loads(currentTime.text)
   symbol = symbol.upper()
   message = y['iso']+ 'GET'+ '/api/account/v3/wallet'

   h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
   encoded = base64.b64encode(h)
   
   
   r = req.get('https://www.okex.com/api/account/v3/wallet', headers={'content-type':'application/json;  charset=UTF-8' ,
                                                                                'OK-ACCESS-KEY': API_Key ,
                                                                                'OK-ACCESS-SIGN': encoded,
                                                                                'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                'OK-ACCESS-PASSPHRASE': 'xinxere8407' })
    
   jsonConvert = json.loads(r.text)
   for balan in jsonConvert:
        if balan['currency']== symbol :
            return float(balan['available']) 

   return "null"
  except Exception as ex : 
       print(str(ex) + ' at GetBalance('+symbol+')')  


def GetBalance(symbol):
  try:
   return float(spotAPI.get_coin_account_info(symbol)['available']) 
  except Exception as ex : 
       print(str(ex) + ' at GetBalance('+symbol+')') 


def BuyOrder(sourceSymbol, tragetSymbol ,volumn,isRebalance):
   try:
      symbol = tragetSymbol+'-'+sourceSymbol
      #price = getRateForBuy(sourceSymbol, tragetSymbol)
      amount = volumn
      if amount == 0:
        amount = GetBalance(sourceSymbol)

      spotAPI = okexSpot.SpotAPI(key1, skey1, 'xinxere8407', True)
      r = spotAPI.take_order('market','buy',symbol,amount)
      
      jsonConvert = json.loads(r.text)
      if 'result' in jsonConvert:
        return jsonConvert['result']

      return False
   except Exception as ex : 
        print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')

def SellOrder(sourceSymbol, tragetSymbol):
  try:
      symbol = tragetSymbol+'-'+sourceSymbol
      #price = getRateForBuy(sourceSymbol, tragetSymbol)
      amount = GetBalance(tragetSymbol)

      spotAPI = okexSpot.SpotAPI(key1, skey1, 'xinxere8407', True)
      r = spotAPI.take_order('market','sell',symbol,amount)
      
      jsonConvert = json.loads(r.text)
      if 'result' in jsonConvert:
        return jsonConvert['result']

      return False
  except Exception as ex : 
        print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')

def transferOrder(symbol,tragetAccount, addressTag=""):
  try:
    transferSpotToFundAccount(symbol)
    tragetAccountNew = tragetAccount
    if(addressTag != ""):
      tragetAccountNew = tragetAccountNew+"："+str(addressTag)

    r = accountAPI.coin_withdraw(symbol, GetBalance(symbol), 4 , tragetAccountNew ,'nampapaya1212' ,GetFeeWithdrawPrice(symbol) )       
    
    return True
  except Exception as ex : 
        print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')

def transferFundAccountToSpot(symbol , volumn=0.0):
  try:
    accountAPI = okexAccount.AccountAPI(API_Key, API_SECRET, 'xinxere8407', True)
    if volumn == 0.0:
       volumn = GetBalanceFund(symbol)

    r = accountAPI.coin_transfer(symbol, volumn, 6 , 1 ) 
    
    return True
  except Exception as ex :
    print(str(ex) + ' at transferFundAccount()')

def transferSpotToFundAccount(symbol , volumn=0.0):
  try:
    
    if volumn == 0.0:
       volumn = GetBalance(symbol)

    r = accountAPI.coin_transfer(symbol, volumn, 1 , 6 ) 
    
    return True
  except Exception as ex :
    print(str(ex) + ' at transferFundAccount()')



def getQuantityCanBuy(sourceSymbol,tragetSymbol):
  try:
    getBalance = GetBalance(sourceSymbol)

    if getBalance == 'null':
        getBalance = 0.0

    jsonConvert = getPriceSymbol(sourceSymbol,tragetSymbol)
    getPrice = float(jsonConvert['asks'][0][0])

    return getBalance/getPrice
  except Exception as ex : 
        print(str(ex) + ' at getQuantityCanBuy('+sourceSymbol+','+tragetSymbol+')')

def getRateForBuy(sourceSymbol, tragetSymbol ):
  try:
    time.sleep(1)
    jsonConvert = getPriceSymbol(sourceSymbol,tragetSymbol)
    rateBuy = float(jsonConvert['asks'][0][0]) * (110/100) 
    return rateBuy
  except Exception as ex : 
        print(str(ex) + ' at getRateForBuy('+sourceSymbol+','+tragetSymbol+')')

def getRateForSale(sourceSymbol, tragetSymbol ):
  try:
    time.sleep(1)
    jsonConvert = getPriceSymbol(sourceSymbol,tragetSymbol)
    rateSale = float(jsonConvert['bids'][0][0]) * (90/100) 
    return rateSale
  except Exception as ex : 
        print(str(ex) + ' at getRateForSale('+sourceSymbol+','+tragetSymbol+')')


def GetFeeWithdrawPrice(symbol):
   try:
    time.sleep(1)
    currentTime = req.get('https://www.okex.com/api/general/v3/time')
    y = json.loads(currentTime.text)

    message = y['iso'] + 'GET' + '/api/account/v3/withdrawal/fee?currency=' + symbol

    h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
    encoded = base64.b64encode(h)
    
    
    r = req.get('https://www.okex.com/api/account/v3/withdrawal/fee?currency=' + symbol, headers={'content-type':'application/json;  charset=UTF-8' ,
                                                                                  'OK-ACCESS-KEY': API_Key ,
                                                                                  'OK-ACCESS-SIGN': encoded,
                                                                                  'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                  'OK-ACCESS-PASSPHRASE': 'xinxere8407' })
    jsonConvert = json.loads(r.text)

    return float(jsonConvert[0]["max_fee"])
   except Exception as ex : 
        print(str(ex) + ' at GetFeePrice('+symbol+')')



def getPriceSymbol(sourceSymbol, tragetSymbol):
    try:
      time.sleep(1)
      currentTime = req.get('https://www.okex.com/api/general/v3/time')
      y = json.loads(currentTime.text)
      symbol = tragetSymbol+'-'+sourceSymbol

      message = y['iso']+ 'GET'+ '/api/spot/v3/instruments/'+symbol+'/book'

      h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
      encoded = base64.b64encode(h)


      r = req.get('https://www.okex.com/api/spot/v3/instruments/'+symbol+'/book', headers={'content-type':'application/json;  charset=UTF-8' ,
                                                                                      'OK-ACCESS-KEY': API_Key ,
                                                                                      'OK-ACCESS-SIGN': encoded,
                                                                                      'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                      'OK-ACCESS-PASSPHRASE': 'xinxere8407' })
      jsonConvert = json.loads(r.text)
      return jsonConvert
    except Exception as ex : 
        print(str(ex) + ' at getPriceSymbol('+sourceSymbol+','+tragetSymbol+')')


def transferOrderByLib(symbol,tragetAccount, addressTag=""):
  try:
    tragetAccountNew = tragetAccount
    if(addressTag != ""):
      tragetAccountNew = tragetAccountNew+"："+str(addressTag)

    accountAPI = okexAccount.AccountAPI(key1, skey1, 'xinxere8407', True)
    r = accountAPI.coin_withdraw(symbol, GetBalance(symbol), 4 , tragetAccountNew ,'nampapaya1212' ,GetFeeWithdrawPrice(symbol) ) 
    print(r.text)       
    
    return False
  except Exception as ex : 
        print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')


def getMinimumBuySource(tragetSymbol):
  try:
    currencies = accountAPI.get_currencies()
    for currency in currencies:
      if currency['currency'] == tragetSymbol:
         return float(currency['min_withdrawal'])
  except Exception as ex : 
        print(str(ex) + ' at getMinimumBuySource('+tragetSymbol+')')



def getMinimumSellSource(sourceSymbol):
  try:
    currencies = accountAPI.get_currencies()
    for currency in currencies:
      if currency['currency'] == sourceSymbol:
        return float(currency['min_withdrawal'])
  except Exception as ex : 
        print(str(ex) + ' at getMinimumSellSource('+sourceSymbol+')')


def CheckPendingOrder(symbol):
  try:
    withdraw = accountAPI.get_coin_withdraw_record(symbol)
    if len(withdraw) > 0:
      if withdraw[0]['status'] == 2:
        return True
    else:
      return False

  except Exception as ex : 
        print(str(ex) + ' at CheckPendingOrder('+symbol+')')



# print(transferOrderByLib("XRP" , "rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs" , "1015165964" ))





# def transferOrder(symbol,tragetAccount, addressTag=""):
#   try:
#     time.sleep(1)
#     currentTime = req.get('https://www.okex.com/api/general/v3/time')
#     y = json.loads(currentTime.text)
    
#     if(addressTag != ""):
#       tragetAccount = tragetAccount+"："+str(addressTag)

#     param = { 'currency': symbol ,
#               'amount': GetBalance(symbol) ,
#               'destination':4,
#               'to_address': tragetAccount,
#               'trade_pwd': 'wktsjz8q',
#               'fee':  GetFeeWithdrawPrice(symbol) 
#             }
#     paramBody = json.dumps(param)        
#     message = y["iso"] + "POST" + "/api/account/v3/withdrawal"+paramBody

#     h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
#     encoded = base64.b64encode(h)
      
      
#     r = req.post('https://www.okex.com/api/spot/v3/orders' ,headers={'content-type':'application/json;  charset=UTF-8' ,
#                                                                                     'OK-ACCESS-KEY': API_Key ,
#                                                                                     'OK-ACCESS-SIGN': encoded,
#                                                                                     'OK-ACCESS-TIMESTAMP': y['iso'],
#                                                                                     'OK-ACCESS-PASSPHRASE': 'xinxere8407' } )

#     jsonConvert = json.loads(r.text)
#     if 'result' in jsonConvert:
#       return jsonConvert['result']
    
#     return False
#   except Exception as ex : 
#         print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')


# def SellOrder(sourceSymbol, tragetSymbol):
#   try:
#       time.sleep(1)
#       currentTime = req.get('https://www.okex.com/api/general/v3/time')
#       y = json.loads(currentTime.text)
#       symbol = tragetSymbol+'-'+sourceSymbol
#       price = getRateForSale(sourceSymbol, tragetSymbol)

#       message = y["iso"] + "POST" + "/api/spot/v3/orders{'type': 'limit', 'side': 'sell', 'instrument_id': "+symbol+", 'size': "+str(GetBalance(sourceSymbol))+",  'price': "+str(price)+",  'margin_trading': 1, 'order_type': '3'}"

#       h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
#       encoded = base64.b64encode(h)
      
      
#       r = req.get('https://www.okex.com/api/spot/v3/orders', headers={'content-type':'application/json;  charset=UTF-8' ,
#                                                                                     'OK-ACCESS-KEY': API_Key ,
#                                                                                     'OK-ACCESS-SIGN': encoded,
#                                                                                     'OK-ACCESS-TIMESTAMP': y['iso'],
#                                                                                     'OK-ACCESS-PASSPHRASE': 'xinxere8407' })
#       jsonConvert = json.loads(r.text)
#       if 'result' in jsonConvert:
#         return jsonConvert['result']

#       return False
#   except Exception as ex : 
#         print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')
