
import requests as req
import json
import hmac
import hashlib
import time
import base64
import urllib
import ApiKeyExchange
import Lib.krakenex.api as krakenexApi

accountAPI = krakenexApi.API(ApiKeyExchange.Kraken_KEY , ApiKeyExchange.Kraken_SECRET)

def GetBalance(symbol):
    try:
        time.sleep(1)
        nonce = time.time()
        data1 = {
            'nonce'  : int(nonce)
        } 
        postdata = urllib.parse.urlencode(data1)
        urlpath = "/0/private/Balance"

        # Unicode-objects must be encoded before hashing
        encoded = (str(data1['nonce']) + postdata).encode()

        message = urlpath.encode() + hashlib.sha256(encoded).digest()

        signature = hmac.new(base64.b64decode(ApiKeyExchange.Kraken_SECRET),
                                message, hashlib.sha512)
        
        sigdigest = base64.b64encode(signature.digest())
        
        
        
        header = { 'API-Key': ApiKeyExchange.Kraken_KEY , 'API-Sign': sigdigest.decode()}
        r = req.post('https://api.kraken.com/0/private/Balance', data = data1,  headers =  header , timeout = None)
        symbol = "X"+symbol
        jsonConvert = json.loads(r.text)
        if 'result' in jsonConvert:
            if symbol in jsonConvert['result']:
                return jsonConvert['result'][symbol]

        return 0.0
    except Exception as ex : 
        print(str(ex) + ' at GetBalance('+symbol+')')  


def transferOrder(symbol,tragetAccount, addressTag=""):
    try:
        time.sleep(1)
        nonce = time.time()
        data = {
            'nonce'  : int(nonce),
            'asset' : 'XRP'
        }

        postdata = urllib.parse.urlencode(data)
        urlpath = "/0/private/DepositMethods"

        # Unicode-objects must be encoded before hashing
        encoded = (str(data['nonce'])  + postdata).encode()

        message = urlpath.encode() + hashlib.sha256(encoded).digest()

        signature = hmac.new(base64.b64decode(ApiKeyExchange.Kraken_SECRET),
                                message, hashlib.sha512)
        
        sigdigest = base64.b64encode(signature.digest())
        
        
        
        header = { 'API-Key': ApiKeyExchange.Kraken_KEY , 'API-Sign': sigdigest.decode()}
        r = req.get('https://api.kraken.com/0/private/DepositMethods', data = data,  headers =  header , timeout = None) 

        print(r.text)
    except Exception as ex : 
        print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')  

def buyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance):
    try:
        #currencyPair = tragetSymbol+sourceSymbol
        balance = volumn
        if not isRebalance:
            balance = GetBalance(sourceSymbol)

        r = accountAPI.query_private('AddOrder' , {'pair':  tragetSymbol+sourceSymbol,'type': 'buy' , 'ordertype':'market' , 'volume' : balance } )

        # jsonLoad = json.loads(r.text)
        # if 'txid' in jsonLoad:
        #     return True

        return r
    except Exception as ex : 
        print(str(ex) + ' at buyOrder('+sourceSymbol+','+tragetSymbol+','+volumn+','+isRebalance+')')        
      
#transferOrder("symbol","tragetAccount", "")
print(buyOrder('usdt' , 'btc' , 0 , False ))




