from Lib.huobi_Python.huobi.requstclient import RequestClient
from Lib.huobi_Python.huobi.model import *

api_key ="4a2b9816-ghxertfvbf-0de7dedf-35676"
secret_ket = "8e8cb614-f132429b-60ec25c8-eb3ac"
client = RequestClient(api_key = api_key,secret_key=secret_ket)
account_id = "6798498"

def GetBalance(symbol):
        try :
            symbol = symbol.lower()
            all_balance = client.get_account_balance()
            for each_coin in range(0,len(all_balance[0].balances)-1) :
                    if  symbol == all_balance[0].balances[each_coin].currency:
                            return float(all_balance[0].balances[each_coin].balance)
            return False
        
        except Exception as ex:
                print(str(ex)+"at GetBalance("+symbol+")")




def BuyOrder(sourceSymbol, tragetSymbol,isRebalance,volume=""):
        #[Example]
        #               checkboolean = BuyOrder('usdt', 'xrp',True,volume=999)
        #               checkboolean = BuyOrder('usdt', 'xrp',False)
        #               return boolean
        try :
                mintrade_size_USDT = 7
                market = str(tragetSymbol+sourceSymbol)
                price_depth =client.get_price_depth(market)
                i=0
                ACCOUNT_TYPE = AccountType.SPOT
                ORDER_TYPE   = OrderType.BUY_LIMIT
                
                if isRebalance == True :
                        all_amount = volume
                        fee_trade = all_amount*GetTradeFee()/100
                        volume_after_paid_fee = all_amount - fee_trade
                        while True:
                                qty_from_order  = price_depth.asks[i].amount
                                rate            = price_depth.asks[i].price
                                value_order     = qty_from_order*rate
                                if value_order >= volume_after_paid_fee :
                                        qty = round(volume_after_paid_fee/rate,2)
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount =qty,
                                                                        price = rate
                                                                      )
                                        break
             
                                else :
                                        qty_from_order  =round(price_depth.asks[i].amount,2)
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount = qty_from_order,
                                                                        price = rate
                                                                      )
                                        volume_after_paid_fee  = volume_after_paid_fee - value_order
                                        i=i+1
                                        
                                if volume_after_paid_fee < mintrade_size_USDT : break

                                
                else :
                        all_amount  = GetBalance(sourceSymbol)
                        fee_trade = all_amount*GetTradeFee()/100
                        volume_after_paid_fee = all_amount - fee_trade
                        while True:
                                qty_from_order  = price_depth.asks[i].amount
                                rate            = price_depth.asks[i].price
                                value_order     = qty_from_order*rate
                                if value_order >= volume_after_paid_fee :
                                        qty = round(volume_after_paid_fee/rate,2)
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount =qty,
                                                                        price = rate
                                                                      )
                                        break
             
                                else :
                                        qty_from_order  =round(price_depth.asks[i].amount,2)
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount = qty_from_order,
                                                                        price = rate
                                                                      )
                                        volume_after_paid_fee  = volume_after_paid_fee - value_order
                                        i=i+1
                                        
                                if volume_after_paid_fee < mintrade_size_USDT : break


                if str(status).isnumeric():
                        checkboolean = True
                return status,checkboolean
        
        except Exception as ex :
                print(str(ex)+"at BuyOrder("+sourceSymbol+","+tragetSymbol+","+str(isRebalance)+","+volume+")")
                return ex,False


        
##def GetTradeStatus(tragetSymbol):
##        try :
##               amount_COIN = len(public_API.get_currencies()[1])-1;
##               for a in range(0,amount_COIN):
##                        if public_API.get_currencies()[1][a]["Currency"] == tragetSymbol:
##                                status = public_API.get_currencies()[1][a]["IsActive"]
##                                return status
##        except Exception as ex:
##                print(str(ex)+'at GetTradeStatus('+tragetSymbol+')')
##


def SellOrder(sourceSymbol , tragetSymbol):
        #[Example]
        #               checkboolean = SellOrder('xrp','usdt')
        #               return boolean
        try :
                all_amount  = GetBalance(sourceSymbol)
                market = str(sourceSymbol+tragetSymbol)
                best_quote = client.get_best_quote(market)
                ACCOUNT_TYPE = AccountType.SPOT
                ORDER_TYPE   = OrderType.SELL_LIMIT
                fee_trade       = all_amount*GetTradeFee()/100
                AMOUNT       =   round(all_amount-fee_trade,2)
                PRICE        = round(best_quote.bid_price*(91/100),4)
                status = client.create_order(
                                        symbol = market ,
                                        account_type = ACCOUNT_TYPE ,
                                        order_type = ORDER_TYPE ,
                                        amount = AMOUNT,
                                        price = PRICE
                                      )
                if str(status).isnumeric():
                        checkboolean = True
                return checkboolean
        
        except Exception as ex :
                print(str(ex) , "at SellOrder(" +sourceSymbol+","+tragetSymbol+")")
                return False


def transferOrder(symbol,tragetAccount, addressTag=""):
         #[Example]
         #            status = transferOrder( 'xrp','rPVMhWBsfF9iMXYj3aAzJVkPDTFNSyWdKy','1553529033')
         #            return boolean            
        try :
                all_fee_transfer = GetFeePrice()
                fee_transfer     = all_fee_transfer['transfer'][symbol]
                qty = GetBalance(symbol)
                qty = qty-fee_transfer
                qty = round(qty,4)-0.0001
                print(qty)
                if addressTag != "":
                        try:
                                withdraw_ = client.withdraw(
                                                         address     = tragetAccount,
                                                         amount      = qty,
                                                         currency    = symbol,
                                                         address_tag = address_Tag
                                                         )
                                
                                return str(withdraw_).isnumeric()
                        except Exception as ex :
                                return False
                        
                else :
                        try:
                               withdraw_ = client.withdraw(
                                                        address  = tragetAccount,
                                                        amount   = qty,
                                                        currency = symbol
                                                        )
                               return str(withdraw_).isnumeric()
                        except Exception as ex :
                                return False
                        
        except Exception as ex:
                print(str(ex)+ 'at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')



def GetFeePrice():
    try:
        fee = {'tradeFee':0.2,
        'transfer':{
          'btc':0.0005,
          'eth':0.005,
          'xrp':0.1,
          'ltc':0.001,
          'usdt':1,
          'bch':0.0001,
          'dash':0.002,
          'eos':0.1
          }
        }
        return fee
    except Exception as ex : 
        print(str(ex) + ' at GetFeePrice()')


def CheckPendingOrder(symbol):
        # [Example] CheckPendingOrder('xrp')
        #           return boolean
        #           False คือ ไม่มี pending
        #           True  คือ pendingอยู่
        #           มีการ print แสดง status 
        try :   
               hisw =  client.get_withdraw_history(
                                                   currency= 'xrp',
                                                   from_id= 1,
                                                   size=100
                                                   )
               Val_Pending = hisw[len(hisw)-1].withdraw_state
               print(Val_Pending)
               if str(Val_Pending) == "confirmed" :
                       return False
               else :
                       return True
                
        except Exception as ex :
                      print(str(ex) + ' at CheckPendingOrder('+symbol+')')
    
def GetAccount(symbol=""):
    try :
        if symbol =="XRP":
            return 'rwADVoWvXxAS3d61zpUta7ZuPWsiEQXxPY',"108076"
        elif symbol == 'ETH':
            return "0xa2c8d0d58edb8cfb7b293b85d532fbaf5f48e508",""
        elif symbol == 'LTC':
            return "LP9TrRxt1SiG4dczJq5ko2HUbq6hLoXajU",""
        elif symbol == 'BTC':
            return "1DHNUCpH9N6MadPV69dhiYKzx5TPzmdNqP",""
        elif symbol == 'USDT':
            return '0xa2c8d0d58edb8cfb7b293b85d532fbaf5f48e508',""
    except Exception as ex : 
        print(str(ex) + ' at GetAccount('+symbol+')')




